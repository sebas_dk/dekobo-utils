package utils

import (
	"encoding/binary"
	"time"
)

var (
	// Commands mapping
	cmd = map[CoverCommands]string{
		CoverCommandOpen:         "open",
		CoverCommandClose:        "close",
		CoverCommandStop:         "stop",
		CoverCommandReset:        "reset",
		CoverCommandProgram:      "program",
		CoverCommandProgramOpen:  "progam_open",
		CoverCommandProgramClose: "progam_close",
		CoverCommandReadOpen:     "read_open",
		CoverCommandReadClose:    "read_close",
	}
	cmdRev = make(map[string]CoverCommands, 9)
	// Positions mapping
	pos = map[CoverPositions]string{
		CoverPositionCurrent: "current",
		CoverPositionStopped: "stopped",
		CoverPositionOpen:    "open",
		CoverPositionOpening: "opening",
		CoverPositionClosing: "closing",
		CoverPositionClosed:  "closed",
		CoverPositionProgram: "program",
	}
	posRev = make(map[string]CoverPositions, 6)
	// Motor type mapping
	motorType = map[CoverMotorTypes]string{
		CoverMotor120NM: "120Nm",
		CoverMotor250NM: "250Nm",
		CoverMotor500NM: "500Nm",
	}
	motorTypeRev = make(map[string]CoverMotorTypes, 3)
	// Camera action mapping
	action = map[CameraActions]string{
		CameraStart: "start",
		CameraStop:  "stop",
	}
	actionRev = make(map[string]CameraActions, 2)
	// Wind Beaufort Scale
	beaufort = map[BeaufortScales][]float64{
		Beaufort0:  {0, 2},
		Beaufort1:  {2, 5},
		Beaufort2:  {5, 11},
		Beaufort3:  {11, 19},
		Beaufort4:  {19, 28},
		Beaufort5:  {28, 38},
		Beaufort6:  {38, 49},
		Beaufort7:  {49, 61},
		Beaufort8:  {61, 74},
		Beaufort9:  {74, 88},
		Beaufort10: {88, 102},
		Beaufort11: {102, 117},
		Beaufort12: {117, 200},
	}
)

// CoverCommands the commands send to the cover
type CoverCommands int64

// String convert the cover command to string
func (c CoverCommands) String() string {
	return cmd[c]
}

// ByteSlice convert the cover command to a byte slice
func (c CoverCommands) ByteSlice() []byte {
	b := make([]byte, byteSliceLength)
	binary.LittleEndian.PutUint64(b, uint64(c))
	return b
}

// ConvertStringToCommand convert the string back to a command
func ConvertStringToCommand(s string) CoverCommands {
	if len(cmdRev) == 0 {
		for k, v := range cmd {
			cmdRev[v] = k
		}
	}
	return cmdRev[s]
}

// ConvertByteToCommand convert the byte slice back to a command
func ConvertByteToCommand(b []byte) CoverCommands {
	return CoverCommands(int64(binary.LittleEndian.Uint64(b)))
}

// CoverPositions the possible positions for the cover
type CoverPositions int64

// String convert the cover position to a string
func (c CoverPositions) String() string {
	return pos[c]
}

// ConvertStringToPosition convert the string back to a cover position
func ConvertStringToPosition(s string) CoverPositions {
	if len(posRev) == 0 {
		for k, v := range pos {
			posRev[v] = k
		}
	}
	return posRev[s]
}

// ByteSlice convert the cover position to a byte slice
func (c CoverPositions) ByteSlice() []byte {
	b := make([]byte, byteSliceLength)
	binary.LittleEndian.PutUint64(b, uint64(c))
	return b
}

// ConvertByteToPosition convert the byte slice back to a cover position
func ConvertByteToPosition(b []byte) CoverPositions {
	return CoverPositions(int64(binary.LittleEndian.Uint64(b)))
}

// CoverMotorTypes the motor types for the cover
type CoverMotorTypes int64

// String convert the cover motor type to string
func (c CoverMotorTypes) String() string {
	return motorType[c]
}

// ConvertStringToMotorType convert the string back to a motor type
func ConvertStringToMotorType(s string) CoverMotorTypes {
	if len(motorTypeRev) == 0 {
		for k, v := range motorType {
			motorTypeRev[v] = k
		}
	}
	return motorTypeRev[s]
}

// CameraActions the actions send to the camera
type CameraActions int64

// ConvertByteToAction convert the byte slice back to an action
func ConvertByteToAction(b []byte) CameraActions {
	return CameraActions(int64(binary.LittleEndian.Uint64(b)))
}

// ByteSlice convert the camera action to a byte slice
func (c CameraActions) ByteSlice() []byte {
	b := make([]byte, byteSliceLength)
	binary.LittleEndian.PutUint64(b, uint64(c))
	return b
}

// ConvertStringToAction convert the string back to an action
func ConvertStringToAction(s string) CameraActions {
	if len(actionRev) == 0 {
		for k, v := range action {
			actionRev[v] = k
		}
	}
	return actionRev[s]
}

// MatchBeaufortScale match the wind speed to the beaufort scale
func MatchBeaufortScale(ws float64) BeaufortScales {
	var bs BeaufortScales
	for b, kmhs := range beaufort {
		if ws >= kmhs[0] && ws < kmhs[1] {
			bs = b
		}
	}

	return bs
}

// CoverStates the current state of the cover
type CoverStates int64

// CoverPositionStatus the status of the current cover position
type CoverPositionStatus int64

// CoverPositionCounter counter for the cover position
type CoverPositionCounter int64

// BeaufortScales are the wind scales for the wind sensor
type BeaufortScales int64

const (
	CoverCommandOpen CoverCommands = iota
	CoverCommandClose
	CoverCommandStop
	CoverCommandReset
	CoverCommandProgram
	CoverCommandProgramOpen
	CoverCommandProgramClose
	CoverCommandReadOpen
	CoverCommandReadClose
	CoverCommandReadMoving

	CoverPositionStopped CoverPositions = iota
	CoverPositionOpen
	CoverPositionOpening
	CoverPositionClosing
	CoverPositionClosed
	CoverPositionProgram
	CoverPositionCurrent

	CoverPositionStatusOpen CoverPositionStatus = iota
	CoverPositionStatusClose
	CoverPositionStatusMoving

	CoverPositionCounterOpen CoverPositionCounter = iota
	CoverPositionCounterClosed
	CoverPositionCounterStopped
	CoverPositionCounterOpening
	CoverPositionCounterClosing

	CoverStateCurrentPin CoverStates = iota
	CoverStateStopped
	CoverStateReset
	CoverStateEnabled

	CoverExecutionTime = 200 * time.Millisecond
	CoverResetTime     = 4 * time.Second
	CoverProgramTime   = 1 * time.Second
	CoverNotifyTime    = 1 * time.Second

	byteSliceLength = 8

	CoverMotor120NM CoverMotorTypes = iota
	CoverMotor250NM
	CoverMotor500NM

	CameraStart CameraActions = iota
	CameraStop

	CoverCommandEnable CoverCommands = iota
	CoverCommandDisable
)

const (
	Beaufort0 BeaufortScales = iota
	Beaufort1
	Beaufort2
	Beaufort3
	Beaufort4
	Beaufort5
	Beaufort6
	Beaufort7
	Beaufort8
	Beaufort9
	Beaufort10
	Beaufort11
	Beaufort12
)

const (
	// SocketWriteWaitTime time allowed to write a message to the peer.
	SocketWriteWaitTime = 10 * time.Second

	// SocketPongWaitTime time allowed to read the next pong message from the peer.
	SocketPongWaitTime = 60 * time.Second

	// SocketPingPeriod Send pings to peer with this period. Must be less than pongWait.
	SocketPingPeriod = (SocketPongWaitTime * 9) / 10

	// SocketMaxMessageSize Maximum message size allowed from peer.
	SocketMaxMessageSize = 15
)
